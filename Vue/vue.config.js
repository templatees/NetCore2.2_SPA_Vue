module.exports = {
    baseUrl: './',
    assetsDir: 'static',
    productionSourceMap: false,
    devServer: {
        host: '127.0.0.1',
        port: 9090,
    },
    //关闭语法验证
    lintOnSave:false
}