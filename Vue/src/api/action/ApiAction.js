
import {Controller} from "../stdlib/ApiRequestAction"


//用户
const UserController = new Controller("User");
/**api路由 规则 */
export const UserAction = {
    GetAll: UserController.Get("getall"),
    /**登录 */
    Login: UserController.Post("login"),
    /**注册*/
    Register: UserController.Post("Register"),

     /**登录 */
    GetList: UserController.Post("getlist"),
    Add: UserController.Post("add"),
    Delete: UserController.Post("delete"),
    Update: UserController.Post("update"),
    GetById:UserController.Get("getbyid"),

};

/**商品类型 */
const GoodsTypeController = new Controller("GoodsType");
export const GoodsTypeAction = {
    GetAll: GoodsTypeController.Get("getall"),
    GetList: GoodsTypeController.Post("getlist"),

    Add: GoodsTypeController.Post("add"),
    Delete: GoodsTypeController.Post("delete"),
    Update: GoodsTypeController.Post("update"),
    GetById:GoodsTypeController.Get("getbyid"),
};

/**商品 */
const GoodsController = new Controller("Goods");
export const GoodsAction = {
    GetAll: GoodsController.Get("getall"),
    GetListByType: GoodsController.Get("getlistbytype"),
    GetList: GoodsController.Post("getlist"),
    Add: GoodsController.Post("add"),
    Delete: GoodsController.Post("delete"),
    Update: GoodsController.Post("update"),
    GetById:GoodsController.Get("getbyid"),
};

/**权限角色 */
const RoleController = new Controller("Role");
export const RoleAction = {
    GetAll: RoleController.Get("getall"),
    GetList: RoleController.Post("getlist"),
    Add: RoleController.Post("add"),
    Delete: RoleController.Post("delete"),
    Update: RoleController.Post("update"),
    GetById:RoleController.Get("getbyid"), 
    rolebindresources:RoleController.Get("rolebindresources"),

};

/**资源 */
const ResourcesController = new Controller("Resources");
export const ResourcesAction = {
    GetAll: ResourcesController.Get("getall"),
    GetList: ResourcesController.Post("getlist"),
    Add: ResourcesController.Post("add"),
    Delete: ResourcesController.Post("delete"),
    Update: ResourcesController.Post("update"),
    GetById:ResourcesController.Get("getbyid"),
    GetRoleResources:ResourcesController.Get("GetRoleResources"), 
    GetAllResourcesDto:ResourcesController.Get("GetAllResourcesDto"),
};


 
const RelationRoleResourcesController = new Controller("RelationRoleResources");
export const RelationRoleResourcesAction = {
    GetAll: RelationRoleResourcesController.Get("getall"),
    GetList: RelationRoleResourcesController.Post("getlist"),
    Add: RelationRoleResourcesController.Post("add"),
    Delete: RelationRoleResourcesController.Post("delete"),
    Update: RelationRoleResourcesController.Post("update"),
    GetById:RelationRoleResourcesController.Get("getbyid"), 
    rolebindresources:RelationRoleResourcesController.Post("rolebindresources"),

};

 
const BillItemController = new Controller("BillItem");
export const BillItemAction = {
    GetAll: BillItemController.Get("getall"),
    GetList: BillItemController.Post("getlist"),
    Add: BillItemController.Post("add"),
    Delete: BillItemController.Post("delete"),
    Update: BillItemController.Post("update"),
    GetById:BillItemController.Get("getbyid"), 
    getyear:BillItemController.Get("getyear"),

};
