import { HttpRequest, HttpMethod } from "./ApiRequest";
import { createResponse } from "./ApiResponse";
import cacheUtil from "../../utils/cacheUtil";
import ObjectUtil from "../../utils/ObjUtil";

import bus from "../../utils/bus"


/** 
  * apiaction为ApiRequestAction，
  * data 请求参数，
  *isqueystring在get时可以使用
  * */
const baseRequest = async function (apiaction, data, isqueystring) {
    bus.loading();//这个是自定义的
    if (!ObjectUtil.isObject(apiaction)) {
        console.log('apiaction不是ApiRequestAction')
        return;
    }
    if (ObjectUtil.IsNullOrWhiteSpace(data)) {
        data = {};
    }
    let url = '/' + apiaction.ctrl + '/' + apiaction.action;

    let res = new HttpRequest();
    res.setUrl(url);
    //有tokenjiatoken
    if (cacheUtil.getToken()) {
        res.AddHeaders({ 'Authorization': cacheUtil.getToken() });
    }

    const postdata = JSON.parse(JSON.stringify(data));
    res.setMethod(apiaction.method);
    if (apiaction.method == HttpMethod.GET && isqueystring) {
        res.setDataParams(postdata);
    } else {
        res.setDataDatas(postdata);
    }
    let resData = await res.Request();
    bus.closeLoading();////这个是自定义的
    return createResponse(resData);
}

/**serve 基类*/
export class BaseSend {
    /** 
  * apiaction为ApiRequestAction，
  * data 请求参数，
  *isqueystring在get时可以使用
  * */
    async Send(apiaction, data, isqueystring) {
        return await baseRequest(apiaction, data, isqueystring)
    }
    /**获取分页 */
    // getPageData(index, size, Param) {
    //     return new PageParam(index, size, Param);
    // }
}

/**请求分页参数格式 */
export class PageParam {
    PageIndex = 1;
    PageSize = 10;
    ParamWhere = "";
    constructor(index, size, Param) {
        if (index) {
            this.PageIndex = index;
        }
        if (size) {
            this.PageSize = size;
        }
        if (Param) {
            this.ParamWhere = Param;
        }

    }

    pageAdd() {
        this.PageIndex++;
    }
    setParams(param) {
        this.ParamWhere = param
    }
    addParams(param) {
        this.ParamWhere = this.ParamWhere + '  and  ' + param;
    }
    setIndex(index) {
        this.PageIndex = index;
    }
}