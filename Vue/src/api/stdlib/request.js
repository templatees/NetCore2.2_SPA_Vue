import axios from 'axios'
import ApiConfig from '../ApiConfig'

// 配置项目根如路径

// axios请求
function httpApi(method, url, params) {
    return new Promise((resolve, reject) => {
        axios({
            method: method,
            url: url,
            data: method === 'POST' || method === 'PUT' ? params : null,
            params: method === 'GET' || method === 'DELETE' ? params : null,
            baseURL: ApiConfig.baseUrl(),
            timeout: ApiConfig.timeOut(),
        }).then(
            (response) => {
                resolve(response)
            }
        ).catch(
            (error) => {
                reject(error)
            }
        )
    })
}


const requestCommon = {}

requestCommon.post = function (url, data) {
     return httpApi('POST', url, data)
}

requestCommon.get = function (url, data) {
    return httpApi('GET', url, data)
}
/**每个请求 都走了这里 */
requestCommon.axios = function (obj) {
    return  axios(obj);
}


// 　　拦截器
// 　　axios的拦截器就是在请求发送前做些处理，然后发送处理过后的请求。
// 　　Axios.interceptors.request.use(function(config){
//          //做一些处理
//          return config;  //然后返回
//     });
//    //得到响应后，在做一些处理，然后返回处理后的结果。
// 　　Axios.interceptors.response.use(function(config){
//          //做一些处理
//          return config;  //然后返回
//     });

export default requestCommon;