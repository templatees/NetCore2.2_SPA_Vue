import { HttpMethod } from "./ApiRequest";

class ApiAction {
    constructor(action, ctrl, method) {
        this.action = action;
        this.ctrl = ctrl;
        this.method = method;
    }
}

/**默认GET */
function newAction(action, ctrl, method) {
    method = method ? method : "GET";
    return new ApiAction(action, ctrl, method);
}

/**控制器 */
class Controller {
    constructor(ctrl) {
        let ct = ctrl;
        this.Action = (action, method) => {
            return newAction(action, ct, method);
        }
        this.Get = (action) => {
            return newAction(action, ct, HttpMethod.GET);
        }
        this.Post = (action) => {
            return newAction(action, ct, HttpMethod.POST);
        }
    }
}

export {Controller}