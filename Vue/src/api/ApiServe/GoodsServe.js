import { GoodsTypeAction, GoodsAction } from "../action/ApiAction";
import { BaseServe } from "./BaseServe";

class GoodsServe   {
    constructor() {
        this.BaseGoods=new BaseServe(GoodsAction);
        this.BaseGoodsType=new BaseServe(GoodsTypeAction);
    }
}
 

/**
 * spend类服务函数
 */
export default new GoodsServe;