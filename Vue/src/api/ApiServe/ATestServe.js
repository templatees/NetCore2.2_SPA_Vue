import { UserAction, GoodsTypeAction, GoodsAction } from "../action/ApiAction";
import cache from "../../utils/cacheUtil";
import { BaseServe } from "./BaseServe";


class ATestServe {
    constructor() {
        this.BaseServe=new BaseServe(GoodsAction);
    }
}

export default new ATestServe;