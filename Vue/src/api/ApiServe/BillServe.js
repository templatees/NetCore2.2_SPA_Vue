import {  BillItemAction   , GoodsAction } from "../action/ApiAction";
import { BaseServe } from "./BaseServe";

class BillServe   {
    constructor() {
        this.BaseServe =new BaseServe(BillItemAction);
    }

    async  GetYear(year, succesBack, errorBack){
        let response = await this.BaseServe.BaseSend.Send(BillItemAction.getyear, {month:year},true);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }

}
 

/**
 * spend类服务函数
 */
export default new BillServe;