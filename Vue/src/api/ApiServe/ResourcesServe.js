import { ResourcesAction } from "../action/ApiAction";
import { BaseServe } from "./BaseServe";

class ResourcesServe {
    constructor() {
        this.BaseServe = new BaseServe(ResourcesAction);
    }

    async  GetRoleResources(roleid, succesBack, errorBack){
        let response = await this.BaseServe.BaseSend.Send(ResourcesAction.GetRoleResources, {roleid:roleid},true);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }

    async  GetAllResourcesDto(succesBack, errorBack){
        let response = await this.BaseServe.BaseSend.Send(ResourcesAction.GetAllResourcesDto);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }

    

}


export default new ResourcesServe;