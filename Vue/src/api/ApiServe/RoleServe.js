import { RoleAction,RelationRoleResourcesAction } from "../action/ApiAction";
import { BaseServe } from "./BaseServe";

class RoleServe {
    constructor() {
        this.BaseServe = new BaseServe(RoleAction);
    }
    
    async rolebindresources(data, succesBack, errorBack) {
        let postData={
            RoleId:data.RoleId,
            ResourcesIds:data.ResourcesIds
        }
        let response = await this.BaseServe.BaseSend.Send(RelationRoleResourcesAction.rolebindresources, postData);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }

    GetRoleResources
}


export default new RoleServe;