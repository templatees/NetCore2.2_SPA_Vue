import FixedList from './Index.vue';

FixedList.install = function (Vue) {
    Vue.component(FixedList.name, FixedList);
};
export default FixedList;