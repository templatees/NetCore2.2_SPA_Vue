import fixedcontainer from './Index.vue';

fixedcontainer.install = function (Vue) {
    Vue.component(fixedcontainer.name, fixedcontainer);
};
export default fixedcontainer;