import ObjectUtil from "../../../utils/ObjUtil";

 

 /**数 */
var number = (rule, value, callback) => {
    if (ObjectUtil.IsNullOrWhiteSpace(value)) {
        callback(new Error('不能为空'));
    } else if (isNaN(value)) {
        callback(new Error('请输入数值类型'));
    } else {
        callback();
    }
};
/**正数 */
var numbersize = (rule, value, callback) => {
    if (ObjectUtil.IsNullOrWhiteSpace(value)) {
        callback(new Error('不能为空'));
    } else if (isNaN(value)) {
        callback(new Error('请输入数值类型'));
    }
    else if (value <= 0) {
        callback(new Error('必须大于0'));
    }
    else {
        callback();
    }
};

/**不为空 */
var string = (rule, value, callback) => {
    if (ObjectUtil.IsNullOrWhiteSpace(value)) {
        callback(new Error('不能为空'));
    }
    else { callback() }
};

/**去空 不为空 */
var trim = (rule, value, callback) => {
    try{
        let val=value.trim() || "";
        if (ObjectUtil.IsNullOrWhiteSpace(value)) {
            callback(new Error('不能为空'));
        }
        else { callback() }
    }catch{
        callback(new Error('不能为空'));
    }
};


/**手机号 */
var phone = (rule, value, callback) => {
    try{
        let isphone= /(^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$)|(^0{0,1}1[3|4|5|6|7|8|9][0-9]{9}$)/.test(value);
        if(!isphone){
            callback(new Error('格式不对'));
        }else { callback() }
    }catch{
        callback(new Error('格式不对'));
    }
};
 
 
 
 
 
 const Validate= {}

/**数 */
Validate.number = [
    { required: true, validator: number, trigger: 'blur' }
]



/**正数 */
Validate.numbersize = [
    { required: true, validator: numbersize, trigger: 'blur' }
]



/**不为空 */
Validate.string =  [
    { required: true, validator: string, trigger: 'blur' }
]
/**去空 不为空 */
Validate.trim =  [
    { required: true, validator: trim, trigger: 'blur' }
]

/**手机号 */
Validate.phone =  [
    { required: true, validator: phone, trigger: 'blur' }
]

export {Validate}