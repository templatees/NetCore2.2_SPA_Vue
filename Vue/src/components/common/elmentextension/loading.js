import { Loading } from 'element-ui';
import Vue from 'vue';

//vue扩展

Vue.prototype.loading=function(){
    Loading.service({
        text:"拼命加载中",
        spinner:"el-icon-loading",
        background:"rgba(0, 0, 0, 0.8)"
        })
}

Vue.prototype.closeLoading=function(){
    Loading.service({
        text:"拼命加载中",
        spinner:"el-icon-loading",
        background:"rgba(0, 0, 0, 0.8)"
        }).close();
}



