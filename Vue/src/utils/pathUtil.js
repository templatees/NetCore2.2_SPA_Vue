import { baseVm } from "./basevm";


/***路由 跳转  */
class toPageUtil {
    vm = null;
    constructor() {
        if (this.vm == null) {
            this.vm = baseVm();
        }
    }
    /**页面跳转（URL，参数） */
    ToPath(path, params) {

        let pathStr = this.vm.baseRoute.app._route.path;
        if (pathStr == path) {
            return;
        }
        // escape()和 unescape()
        if (params) {
            params = escape(JSON.stringify(params));
            this.vm.baseRoute.push({
                path: path,
                query: { params }
            });
        }else{
            this.vm.baseRoute.push({
                path: path,
            });
        }

    }
    ToPathName(path) {

        let pathStr = this.vm.baseRoute.app._route.name;

        if (pathStr == path) {
            return;
        }
        this.vm.baseRoute.push({ name: path });

    }
    /**获取当前页面  url传参的 参数  */
    getUrlParams() {
        let params = this.vm.baseRoute.app._route.query;
        let lenth = Object.keys(params);
        if (lenth.length > 0) {
            return JSON.parse(unescape(params.params));
        }
        console.error("页面：" + this.vm.baseRoute.app._route.path + ",无url参数。不要使用toPageUtil.getUrlParams()方法");
        return "";
    }
}




export default new toPageUtil;