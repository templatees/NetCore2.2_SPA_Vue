import linq from './linq.min.js'

/**删除数组中的某一个对象
   _arr:数组
   _obj:需删除的对象
   */
function removeArray(_arr, _obj) {
    var length = _arr.length;
    for (var i = 0; i < length; i++) {
        let a = JSON.stringify(_arr[i]).trim()
        let b = JSON.stringify(_obj).trim()
        if (a == b) {
            if (i == 0) {
                _arr.shift(); //删除并返回数组的第一个元素
                return _arr;
            }
            else if (i == length - 1) {
                _arr.pop();  //删除并返回数组的最后一个元素
                return _arr;
            }
            else {
                _arr.splice(i, 1); //删除下标为i的元素
                return _arr;
            }
        }
    }
}


class Enumerable {
    list = [];
    constructor(arr) {
        this.list = arr || [];
        return this;
    }

    Add(itme) {
        this.list.push(itme);
    }

    AddRange(list2) {
        this.list.concat(list2);
    }

    Remove(itme) {
        removeArray(this.list, itme);
    }
    
    Any(fun) {
        let type = Object.prototype.toString.call(fun);
        if(type=="[object Function]"){
            return this.list.any(fun);
        }
    }
    
    Where(fun) {
        let type = Object.prototype.toString.call(fun);
        if(type=="[object Function]"){
            return this.list.where(fun);
        }
    }
    
    Select(fun){
        let type = Object.prototype.toString.call(fun);
        if(type=="[object Function]"){
            return this.list.select(fun);
        }
    }

}

export function LinqFrom(arr) {
    return new Enumerable(arr);
}


