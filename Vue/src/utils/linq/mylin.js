
/**
 * 遍历数组
 * @param {*} func 
 */
Array.prototype.each = function (callback, context) {
    for (var i = 0, l = this.length; i < l; i++) callback.call(context || window, this[i], i, this);
}

/**
 * 遍历的同时清空数组
 * @param {*} func 
 */
Array.prototype.eachShift = function (func) {
    while (this.length) func(this.shift());
}


/**
 * 查询返回新的结果集合
 */
Array.prototype.select = Array.prototype.map || function (selector, context) {
    var i = 0,
        arr = [],
        l = this.length;
    for (i; i < l; i++) arr.push(selector.call(context || window, this[i], i, this));
    return arr;
}

/**
 * 过滤查询满足条件的集合
 */
Array.prototype.where = Array.prototype.filter || function (predicate, context) {
    var i = 0,
        arr = [],
        l = this.length;
    for (i; i < l; i++)
        if (predicate.call(context || window, this[i], i, this) === true) arr.push(this[i]);
    return arr;
}

/**
 * 有一项满足条件侧返回true
 */
Array.prototype.any = Array.prototype.some || function (predicate, context) {
    var l = this.length;
    if (!predicate) return l > 0;
    while (l-- > 0)
        if (predicate.call(context || window, this[l], l, this) === true) return true;

    return false;
}

/**
 * 每一项都满足条件才返回true
 */
Array.prototype.all = Array.prototype.every || ((predicate, context) => this.length == this.where(predicate, context || window).length);

/**
 * 正向排序
 * @param {排序条件} clause 
 */
Array.prototype.orderBy = function (clause, comparer) {
    var arr = this.slice(0);

    comparer = comparer || ((a, b, c) => {
        var x = (c || clause).apply(a, [a]),
            y = (c || clause).apply(b, [b]);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });

    let fn = (a, b) => comparer(a, b);

    arr.thenBy = function (clause1) {
        return arr.orderBy(clause1, function (a, b) {
            var res = fn(a, b);
            return res === 0 ? comparer(a, b, clause1) : res;
        });
    };

    arr.thenByDescending = function (clause2) {
        return arr.orderBy(clause2, function (a, b) {
            var res = fn(a, b);
            return res === 0 ? -comparer(a, b, clause2) : res;
        });
    };
    arr.sort(fn);
    return arr;
}

/**
 * 逆向排序
 * @param {排序条件} clause 
 */
Array.prototype.orderByDescending = function (clause, comparer) {
    var arr = this.slice(0);

    comparer = comparer || ((a, b, c) => {
        var x = (c || clause).apply(b, [b]),
            y = (c || clause).apply(a, [a]);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });

    let fn = (a, b) => comparer(a, b);

    arr.thenBy = function (clause1) {
        return arr.orderBy(clause1, function (a, b) {
            var res = fn(a, b);
            return res === 0 ? -comparer(a, b, clause1) : res;
        });
    };

    arr.thenByDescending = function (clause2) {
        return arr.orderBy(clause2, function (a, b) {
            var res = fn(a, b);
            return res === 0 ? comparer(a, b, clause2) : res;
        });
    };
    arr.sort(fn);
    return arr;
}

/**
 * 分组
 * @param {分组条件} clause 
 */
Array.prototype.groupBy = function (clause) {
    var len = this.length;
    var arr = [];
    if (typeof (clause) == "function") {
        for (var index = 0; index < len; index++) {

            var key = clause(this[index], index);
            var isExsits = false;

            for (var n = 0; n < arr.length; n++) {
                if (arr[n].key == key) {
                    arr[n].count = parseInt(arr[n].count) + 1;
                    arr[n].groups.push(this[index]);
                    isExsits = true;
                }
            }
            if (!isExsits) {
                arr.push({ key: key, count: 1, groups: [this[index]] });
            }

        }
    }
    return arr;
};

/**
 * 删除项
 * @param {*} item 
 */
Array.prototype.remove = function (item) {
    var i = this.indexOf(item);
    if (i != -1)
        this.splice(i, 1);
}

/**
 * 删除满足条件的所有项
 * @param {*} item 
 */
Array.prototype.removeAll = function (clause) {
    var i = this.length;
    while (i--) {
        if (clause(this[i])) {
            this.splice(i, 1);
        }
    }
    return this;
}

/**
 * 查询多个
 */
Array.prototype.selectMany = function (clause) {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        arr = arr.concat(clause.apply(this[i], [this[i]]));
    }
    return arr;
};

/**
 * 去重
 */
Array.prototype.distinct = function () {
    var len = this.length,
        set = new Set();
    for (var i = 0; i < len; i++) {
        set.add(this[i]);
    }
    return Array.from(set);
};

/**
 * 取满足条件的第一条
 * @param {*} clause 
 */
Array.prototype.first = function (clause) {
    if (clause !== undefined) {
        return this.where(clause).first();
    } else {
        if (this.length > 0) {
            return this[0];
        } else {
            return null;
        }
    }
};

/**
 * 取满足条件的最后一条
 * @param {*} clause 
 */
Array.prototype.last = function (clause) {
    if (clause !== undefined) {
        return this.where(clause).last();
    } else {
        if (this.length > 0) {
            return this[this.length - 1];
        } else {
            return null;
        }
    }
};

/**
 * 取指定数量数据
 * @param {*} count 
 */
Array.prototype.take = n => this.slice(0, n);

/**
 * 跳过指定数量数据
 * @param {*} count 
 */
Array.prototype.skip = n => this.slice(n);


if (typeof Array.prototype.indexOf != "function") {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length >>> 0;
        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

if (typeof Array.prototype.map != "function") {
    Array.prototype.map = function (fn, context) {
        var arr = [];
        if (typeof fn === "function") {
            for (var k = 0, length = this.length; k < length; k++) {
                arr.push(fn.call(context, this[k], k, this));
            }
        }
        return arr;
    };
}

if (typeof Array.prototype.filter != "function") {
    Array.prototype.filter = function (fun /*, thisp */) {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function")
            throw new TypeError();

        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

if (typeof Array.prototype.duplicate != "function") {
    Array.prototype.duplicate = function (compareFucn) {
        var tmp = [];
        this.concat().sort().sort(function (a, b) {
            if (Object.prototype.toString.call(compareFucn) == "[object Function]") {
                if (compareFucn(a, b)) tmp.push(a);
            } else {
                if (a == b && tmp.indexOf(a) === -1) tmp.push(a);
            }
        });
        return tmp;
    }
}

//string扩展方法
String.prototype.PadLeft = function (len, charStr) {
    var s = this + '';
    return new Array(len - s.length + 1).join(charStr, '') + s;
}
String.prototype.PadRight = function (len, charStr) {
    var s = this + '';
    return s + new Array(len - s.length + 1).join(charStr, '');
}
String.prototype.trim = function (char, type) {
    if (char) {
        if (type == 'left') {
            return this.replace(new RegExp('^\\' + char + '+', 'g'), '');
        } else if (type == 'right') {
            return this.replace(new RegExp('\\' + char + '+$', 'g'), '');
        }
        return this.replace(new RegExp('^\\' + char + '+|\\' + char + '+$', 'g'), '');
    }
    return this.replace(/^\s+|\s+$/g, '');
};

String.prototype.startWith = function (str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
}

String.prototype.endWith = function (str) {
    var reg = new RegExp(str + "$");
    return reg.test(this);
}

//全局替换
String.prototype.replaceAll = function (f, e) { //吧f替换成e
    var reg = new RegExp(f, "g"); //创建正则RegExp对象   
    return this.replace(reg, e);
}

// 得到字节长度 
String.prototype.realLength = function () {
    return this.replace(/[^x00-xff]/g, "--").length;
};

String.prototype.toUpperCaseForEachWord = function () {
    return this.toLowerCase().replace(/( |^)[a-z]/g, (L) => L.toUpperCase());
}

/**首个字母大写 */
String.prototype.toUpperCaseForFirstWord = function () {
    if (this.length == 1) {
        return this.toUpperCase();
    } else if (this.length > 1) {
        return this.substring(0, 1).toUpperCase() + this.substring(1);
    } else {
        return this;
    }
}

String.prototype.myReplace = function (f, e) { //吧f替换成e
    var reg = new RegExp(f, "g"); //创建正则RegExp对象   
    return this.replace(reg, e);
}
/**包含 */
String.prototype.contains = function (context) {
    return this.search(context) != -1;
}


Number.prototype.toFixed = function (s) {
    return (parseInt(this * Math.pow(10, s) + 0.5) / Math.pow(10, s)).toString();
}


/**
 * 数字是否在指定范围内
 * @param {*} min 
 * @param {*} n 
 * @param {*} max 
 * @param {标记:[0:<=,<=|1:<,<|2:<=,<,3:<,<=]} inclusive 
 */
Number.prototype.inRange = function (min, max, inclusive) {
    if (inclusive == void 0) inclusive = 0;
    switch (inclusive) {
        case 0:
            return min <= this && this <= max;
        case 1:
            return min < this && this < max;
        case 2:
            return min <= this && this < max;
        case 3:
            return min < this && this <= max;
        default:
            return this;
    }
}

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "H+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


const Utils = {
    /*
        empty Guid
    */
    emptyGuid: "00000000-0000-0000-0000-000000000000",
    /*
        empty date
    */
    emptyDate: '0001-01-01T00:00:00',
    /*
        create a new GUID
    */
    newGuid: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    },
    /*
        Asynchronous execution
    */
    lazy: (func, interval) => func(),//window.setTimeout(func, interval || 100),

    /*
   清空页面数据
   */
    clearForm: function (obj) {

        var clear = function (obj) {
            if (!obj || typeof obj != 'object') return;

            for (let i in obj) {
                var type = Object.prototype.toString.call(obj[i]);
                switch (type) {
                    case '[object Object]':
                        clear(obj[i]);
                        break;
                    case '[object Array]':
                        obj[i] = [];
                        break;
                    case '[object String]':
                        obj[i] = '';
                        break;
                    case '[object Number]':
                        obj[i] = '';
                        break;
                    case '[object Boolean]':
                        obj[i] = false;
                        break;
                    case '[object Date]':
                        obj[i] = new Date();
                        break;
                    default: break;
                }
            }
        }

        clear(obj);
    },
    /*
    将列表多选结果转换为主键集合
    */
    selectionsToArr: function (mulSelections, key) {
        var ids = [];

        if (!mulSelections || mulSelections.length <= 0 || !key) return ids;

        ids = mulSelections.map(function (item) { return item[key] });

        return ids;
    },
    /*
    obj is object type
    */
    cloneObj: function (obj) {
        var str, newobj = obj.constructor === Array ? [] : {};

        if (typeof obj !== 'object') {
            return;
        } else if (JSON) {
            str = JSON.stringify(obj), //序列化对象
                newobj = JSON.parse(str); //还原
        } else {
            for (var i in obj) {
                newobj[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i];
            }
        }
        return newobj;
    },
    /*
        is function ?
    */
    isFunction: function (obj) {
        return Object.prototype.toString.call(obj) == "[object Function]";
    },
    /*
        Promise
    */
    promise: function (func) {
        var p = new Promise(function (resolve, reject) {
            //做一些异步操作
            setTimeout(function () {
                var data = func && func() || '';
                resolve(data);
            }, 0);
        });
        return p;
    },
    /*
        是一个座机固号【028-xxxxxxxx】、手机号【11位】或400号码【400xxxxxxx】    
    */
    isPhoneNumber: function (val) {
        return /(^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$)|(^((\(\d{3}\))|(\d{3}\-))?(1[3578]\d{9})$)|(^400(\d{3}\d{4}$))/.test(val);
    },
    /*
        是一个电话号码或手机号    
    */
    isMobile: function (val) {
        return /(^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$)|(^0{0,1}1[3|4|5|6|7|8|9][0-9]{9}$)/.test(val);
    },
    /*
        是一个邮箱
    */
    isMail: function (val) {
        return /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(val);
    },
    /*中文*/
    isChinese: function (str) {
        return /^[\u3220-\uFA29]+$/.test(str);
    },
    isObjectValueEqual: function (a, b) {
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);
        if (aProps.length != bProps.length) {
            return true;
        }
        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];
            if (a[propName] !== b[propName]) {
                return true;
            }
        }
        return false;
    },
    isRegularInt: function (str) {
        return /^[1-9]\d{0,8}$/.test(str);  //长度限制为9位
    },
    /*
        检查两个对象是否相等
    */
    checkObjectEqual: function (a, b) {
        var strA = JSON.stringify(a),
            strB = JSON.stringify(b);
        return strA === strB;
    },
    //整数
    isNumbers: function (str) {
        return /^[1-9]\d*$/.test(str);
    },
    //整数包含0
    isNumbersAndZero: function (str) {
        return /^[0-9]\d*$/.test(str);
    },
    //9位正整数
    isRegularInt: function (str) {
        return /^[1-9]\d{0,8}$/.test(str);  //长度限制为9位
    },
    //浮点数
    isFloat: function (str) {
        return /^[0-9]+.?[0-9]*$/.test(str);
    },
    //手机号
    isPhone: function (str) {
        return /^1(3|4|5|7|8)\d{9}$/.test(str);
    },
    //验证数字0-999999999.999999
    isRegularNumber: function (str) {
        return /^(0|[1-9]\d{0,8})(\.\d{1,6})?$/.test(str);
    },
    isNumOrLetter: function (str) {
        return (/^[0-9a-zA-Z]*$/g).test(str);
    },
    //是否是有效的编码
    isValidCode: function (str) {
        if (str === "" || str == null || str == void 0) return false;

        return str.replace(/^[_\-@a-zA-Z0-9]+/, '').length == 0;
    },
    //判断是否为空
    isNotNull: function (obj) {
        if (typeof obj == "undefined" || obj == null || obj == "") {
            return true;
        } else {
            return false;
        }
    },
    /*
        扩展对象方法
    */
    extend: function (target, options) {
        return $.extend(true, target, options);
    },
    GMTToStr: function (time) {
        try {
            if (time && time != '') {
                let date = new Date(time)
                let Str = date.getFullYear() + '-' +
                    (date.getMonth() + 1).toString().PadLeft(2, '0') + '-' +
                    date.getDate().toString().PadLeft(2, '0') + ' ' +
                    date.getHours().toString().PadLeft(2, '0') + ':' +
                    date.getMinutes().toString().PadLeft(2, '0') + ':' +
                    date.getSeconds().toString().PadLeft(2, '0')
                return Str;
            } else {
                return '';
            }
        } catch (err) {
            return '';
        }
    },
    //单位为公里；计算出来的结果单位为公里；
    GetDistance: function (lat1, lng1, lat2, lng2) {
        var radLat1 = lat1 * Math.PI / 180.0;
        var radLat2 = lat2 * Math.PI / 180.0;
        var a = radLat1 - radLat2;
        var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
        var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
            Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * 6378.137; // EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    },
    getRouter: function (arrayOri) { //传入点集合

        var totalLng = 0;
        var totalLat = 0;
        var sumLngDiff = 0;
        var sumLatDiff = 0;
        //遍历坐标集，获取平均坐标点于两两点经纬度差之和
        for (var i = 0; i < arrayOri.length; i++) {
            var oriStart = arrayOri[i];
            var lngStart = parseFloat(oriStart.lng);
            var latStart = parseFloat(oriStart.lat);
            totalLng += lngStart;
            totalLat += latStart;
            if (i == arrayOri.length - 1) continue;
            var oriEnd = arrayOri[i + 1];
            var lonEnd = parseFloat(oriEnd.lng);
            var latEnd = parseFloat(oriEnd.lat);
            sumLngDiff += Math.abs(lonEnd - lngStart);
            sumLatDiff += Math.abs(latEnd - latStart);
        }

        var aveLng = totalLng / arrayOri.length;
        var aveLat = totalLat / arrayOri.length;

        //理论行驶长度 = （经度总长+纬度总长+对角线总长）/2
        //由于经度总长+纬度总长=理论行驶最大距离
        //对角线总长是理论形式最短距离
        //按照概率平均分布，理论行驶长度 = （理论行驶最大距离 + 理论行驶最小距离）/2

        //沿经度运行距离
        var lngDis = this.GetDistance(aveLat, aveLng, aveLat, aveLng + sumLngDiff);
        var latDis = this.GetDistance(aveLat, aveLng, aveLat + sumLatDiff, aveLng);
        var oriDis = this.GetDistance(aveLat, aveLng, aveLat + sumLatDiff, aveLng + sumLngDiff);
        var theoreticalDis = (lngDis + latDis + oriDis) / 2;
        return theoreticalDis;
    },
    //将浮点数四舍五入，取小数点后n位
    toDecimal: function (x, e) {
        var f_x = parseFloat(x);
        if (isNaN(f_x)) {
            return "";
        }
        var t = 1;
        for (; e > 0; t *= 10, e--);
        for (; e < 0; t /= 10, e++);
        return Math.round(f_x * t) / t;
    },

    //双引号"及打印数据特殊字符处理 单引号\',双引号\",反斜杠\\,换行符\n,回车符\r,制表符\t,退格符\b,换页符\f,垂直制表符\v
    filterSenseChar: function (obj) {
        for (var a in obj) {
            if (typeof (obj[a]) == "object") {
                this.filterSenseChar(obj[a]); //递归遍历
            }
            else {
                if (typeof (obj[a]) === "string")
                    //obj[a] = (obj[a] + "").replace(/null/g, "\"\"").replace(/\\\"/g, "").replace(/\\n/g, "");
                    obj[a] = obj[a].replace(/\"/g, "").replace(/\\\'/g, "").replace(/\\\"/g, "").replace(/\\\\/g, "").replace(/\\n/g, "").replace(/\\r/g, "").replace(/\\t/g, "").replace(/\\b/g, "").replace(/\\f/g, "").replace(/\\v/g, ""); //全部替换为空
                //obj[a] = obj[a].replace(/\"/g, "'").replace(/\\\'/g, "'").replace(/\\\"/g, "'").replace(/\\\\/g, "\\\\\\\\").replace(/\\n/g, "").replace(/\\r/g, "").replace(/\\t/g, "").replace(/\\b/g, "").replace(/\\f/g, "").replace(/\\v/g, "");//部分替换
                //obj[a] = obj[a].replace(/\\/g, "\\\\"); // 统一将\处理成\\  此方式有问题
            }
        }
    },
    /**
       * 多少秒内点击 次点击问题
       * @param fn
       * @param gapTime
       * @returns {Function}
       */
    throttle(fn, gapTime) {

        if (gapTime == null || gapTime == undefined) {
            gapTime = 10000;
        }
        let _lastTime = null;
        let countNum = 0;
        // 返回新的函数
        return function () {
            let _nowTime = +new Date();
            if (_nowTime - _lastTime > gapTime || !_lastTime) {
                fn.apply(this, arguments); //将this和参数传给原函数
                _lastTime = _nowTime;
                countNum = 0;
            } else {
                countNum++;
                if (countNum > 1) {
                    console.log('当前操作过频繁');
                }
            }
        };
    },
};

export default Utils;
