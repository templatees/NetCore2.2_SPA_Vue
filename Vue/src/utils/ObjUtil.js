

const ObjectUtil = {};

const ObjValue = {
    "String": "[object String]",
    "Undefined": "[object Undefined]",
    "Object": "[object Object]",
    "Array": "[object Array]",
    "Number": "[object Number]",
    "Boolean": "[object Boolean]",
    "Function": "[object Function]",
    "Null": "[object Null]"
};

/**获取类型 */
ObjectUtil.getType = function (obj) {
    return Object.prototype.toString.call(obj);
};

/**判断是否为对象*/
ObjectUtil.isObject = function (obj) {
    return ObjectUtil.getType(obj) === ObjValue.Object;
};

/**判断是否为对象*/
ObjectUtil.isString = function (obj) {
    return ObjectUtil.getType(obj) === ObjValue.String;
};

/**判断是否为对象*/
ObjectUtil.isArray = function (obj) {
    return ObjectUtil.getType(obj) === ObjValue.Array;
};

/**判断是否为对象*/
ObjectUtil.isNumber = function (obj) {
    return ObjectUtil.getType(obj) === ObjValue.Number;
};


/**判断是否为函数 */
ObjectUtil.isFunction = function (obj) {
    return ObjectUtil.getType(obj) === ObjValue.Function;
};

/**判断两个任意对象是否相等 类型需要一样 */
ObjectUtil.isEquals = function (obj1, obj2) {
    if (ObjectUtil.getType(obj1) == ObjectUtil.getType(obj2)) {
        if (ObjectUtil.IsNullOrWhiteSpace(obj1) && ObjectUtil.IsNullOrWhiteSpace(obj2)) {
            return true;
        }
        if (JSON.stringify(obj1) == JSON.stringify(obj2)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
};

/**字符串是否包含 */
ObjectUtil.contains = function (str, obj) {
    if (ObjectUtil.isString(str) && ObjectUtil.isString(obj)) {
        return str.search(obj) != -1;
    }
};


/**判断是否为空、null、Undefined */
ObjectUtil.IsNullOrWhiteSpace = function (val) {
    let type = Object.prototype.toString.call(val);
    if (type == ObjValue.Null) {
        return true;
    }
    if (type == ObjValue.Undefined) {
        return true;
    }
    if (type == ObjValue.String) {
        return val == null || val.trim() == "";
    }
    if (type == ObjValue.Object) {
        return val == null || JSON.stringify(val) == "{}";
    }
    if (type == ObjValue.Array) {
        return val == null || JSON.stringify(val) == '[]';
    }
    if (type == ObjValue.Number) {
        return val == null || JSON.stringify(val) == 'NaN';
    }
    return false;
};

/* 是一个电话号码或手机号   */
ObjectUtil.isMobile = function (val) {
    return /(^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$)|(^0{0,1}1[3|4|5|6|7|8|9][0-9]{9}$)/.test(val);
};


/**递归遍历对象中的key(找到对象中Key的值) */
ObjectUtil.getObj_Value = function (data, target) {
    for (const key of Object.keys(data)) {
        if (key === target) {
            return data[key];
        }
        if (ObjectUtil.isObject(data[key])) {
            const result = ObjectUtil.getObj_Value(data[key], target);
            if (typeof result !== "undefined") {
                return result;
            }
        }
    }
};

// ObjectUtil.isExtens = function (data, key_or_value) {
//     if (ObjectUtil.IsNullOrWhiteSpace(data)) {
//         return false;
//     }
//     let str = JSON.stringify(data);
//     return str.search(key_or_value)>-1;
// }

/**
 * data中找到值为 key_val 的对象 并返回
 */
ObjectUtil.getArr_obj = function (data, key_val) {
    if (!ObjectUtil.isObject(data)) {
        return {};
    }
    for (const key in data) {
        if (data[key] == key_val) {
            return data[key];
        }
        if (ObjectUtil.isObject(data[key])) {
            for (const keys in data[key]) {
                if (data[key][keys] == key_val) {
                    return data[key];
                }
            }
        }

    }
    return {};
    // for (const key of Object.keys(data)) {
    //     console.log(key,data[key])
    //     if (data[key] == target) {
    //         return data[key];
    //     }
    //     if (ObjectUtil.isObject(data[key])) {
    //         const result = ObjectUtil.getValue(data[key], target);
    //         if (typeof result !== "undefined") {
    //             return result;
    //         }
    //     }
    // }
};

/**从start取length个 */
ObjectUtil.truncate = function (list, start, length) {
    const arr = [];
    const len = start + length;
    for (let i = 0; i < list.length; i++) {
        if (i >= start && i < len) {
            arr.push(list[i]);
        }
    }
    return arr;
};

ObjectUtil.groupByLength = function (list, len) {
    if (!Util.isArray(list)) {
        return [];
    }
    const size = list.length;
    const newList = [];
    let index = 0;
    let l = [];
    newList.push(l);
    for (let index = 0; index < size; index++) {
        const obj = [index];
        if (index != 0 && index % len == 0) {
            l = [];
            newList.push(l);
        }
        l.push(list[obj]);
    }

    return newList;
};

ObjectUtil.getFieldList = function (list, field) {

    const arr = [];
    for (const key in list) {
        if (list[key][field]) {
            arr.push(list[key][field]);
        }
    }
    return arr;
}

/**数组 按字段 分组*/
ObjectUtil.groupByField = function (list, field) {

    const arr = {};
    for (const key in list) {
        const obj = list[key];
        const v = obj[field];
        if (!arr[v]) {
            arr[v] = [];
        }
        arr[v].push(list[key]);
    }
    return arr;
}


ObjectUtil.specifyKey = function (list, field) {
    const arr = [];
    for (const key in list) {
        arr[list[key][field]] = list[key];
    }
    return arr;
};


/**
 * 
 */
ObjectUtil.isConte = function (item, list) {
    for (let i = 0; i < list.length; i++) {
        if (list[i] == item) {
            return true
        }
    }
    return false;
};



/**删除数组中的某一个对象
_arr:数组
_obj:需删除的对象
*/
ObjectUtil.removeArray = function (_arr, _obj) {
    var length = _arr.length;
    for (var i = 0; i < length; i++) {
        if (_arr[i] == _obj) {
            if (i == 0) {
                _arr.shift(); //删除并返回数组的第一个元素
                return _arr;
            }
            else if (i == length - 1) {
                _arr.pop();  //删除并返回数组的最后一个元素
                return _arr;
            }
            else {
                _arr.splice(i, 1); //删除下标为i的元素
                return _arr;
            }
        }
    }

}


export default ObjectUtil;