const token_key = "_token_key_user_"
const User_key = "_user_key_user_"
const IsMobile_key = "_user_key_IsMobile_key_"
const Login_key = "_user_user_login_info_key_"

const Login_key_sider = "_user_info_sider_"

// 从sessionStorage删除保存的数据
function remove(key) {
    sessionStorage.removeItem(key);
}

// 从sessionStorage删除所有保存的数据
function clear() {
    sessionStorage.clear();
}

function get(key) {
    let data = sessionStorage.getItem(key);
    data=data==null?{}:data;
    try {
        if (typeof data == "object") {
            return data;
        } else {
            return JSON.parse(data);
        }
    }
    catch (err) {
        return data;
    }
   
}
// 保存数据到sessionStorage
function set(key, val) {
    if (typeof val == "object") {
        sessionStorage.setItem(key, JSON.stringify(val));
    } else {
        sessionStorage.setItem(key, val);
    }
}


const cacheUtil = {};

cacheUtil.clear = function () {
    clear();
}


cacheUtil.setUserInfo = function (val) {
    set(User_key, val)
}

cacheUtil.getUserInfo = function () {
    return get(User_key)
}

cacheUtil.setToken = function (val) {
    set(token_key, val)
}
cacheUtil.getToken = function () {
    // if(cacheUtil.getUserInfo()){
    //     return cacheUtil.getUserInfo().Token
    // }
    return get(token_key)
}

cacheUtil.setIsMobile = function (val) {
    set(IsMobile_key, val)
}
cacheUtil.getIsMobile = function () {
    return get(IsMobile_key)
}

cacheUtil.setPasswrod = function (val) {
   let info= get(Login_key) ||{}
   info.passwrod=val;
    set(Login_key, info)
}
cacheUtil.getPasswrod = function () {
    return get(Login_key).passwrod==null ? "" :get(Login_key).passwrod
}
cacheUtil.setAccount = function (val) {
    let info= get(Login_key) ||{}
    info.Account=val;
    set(Login_key, info)
}
cacheUtil.getAccount = function () {
    return get(Login_key).Account==null ?"":get(Login_key).Account
}

cacheUtil.setUserName = function (val) {
    let info= get(Login_key) ||{}
    info.UserName=val;
    set(Login_key, info)
}
cacheUtil.getUserName = function () {
    return get(Login_key).UserName==null ? "" :get(Login_key).UserName
}

cacheUtil.setSider = function (list) {
    set(Login_key_sider, list)
}
cacheUtil.getSider = function () {
    return get(Login_key_sider)==null ? "" :get(Login_key_sider)
}



export default cacheUtil;