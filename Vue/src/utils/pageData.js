
class PageData {

    constructor(index, size) {
        this.PageIndex = index ? index : 1;
        this.PageSize = size ? size : 10;
        this.PageCount = 0;
    }
    PageIndex;
    PageSize;
    PageCount;
    getIndex() {
        return this.PageIndex;
    }
    getSize() {
        return this.PageSize;
    }
    addIndex() {
        this.PageIndex++;
        return this;
    }
    setIndex(index) {
        this.PageIndex = index;
        return this;
    }
    setSize(size) {
        this.PageSize = size;
        return this;
    }
    setCount(count) {
        this.PageCount = count;
        return this;
    }
}
const PageDataUtil = function (index, size) {
    return new PageData(index, size);
}
export default PageDataUtil;