
const pageMethods = {}

pageMethods.Ref = function (component_ref_Name) {
   return this.$refs[component_ref_Name];
}

class pageConfig {
    pageInit = null;
    constructor(page) {
        if (typeof page != 'object') {
            console.log('页面格式问题')
            return;
        }
        if (this.pageInit == null) {
            this.pageInit = page;
        }
        if (!this.pageInit.methods || typeof this.pageInit.methods != 'object') {
            console.log('没有 methods')
            return;
        }

        this.pageInit.methods = {
            ...this.pageInit.methods,
            ...pageMethods
        }
        return this.pageInit;
    }
}

export default pageConfig;
