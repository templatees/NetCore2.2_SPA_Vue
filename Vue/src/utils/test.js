import { LinqFrom } from "./linq/linqextension"
import ObjectUtil from '../utils/ObjUtil'

export const baseVm = function () {
  var jsonArray = [
    { "user": { "id": 100, "screen_name": "d_linq" }, "text": "to objects" },
    { "user": { "id": 130, "screen_name": "c_bill" }, "text": "g" },
    { "user": { "id": 155, "screen_name": "b_mskk" }, "text": "kabushiki kaisha" },
    { "user": { "id": 301, "screen_name": "a_xbox" }, "text": "halo reach" }
  ]
  // ["b_mskk:kabushiki kaisha", "c_bill:g", "d_linq:to objects"]
  // var queryResult = jsonArray.where(x=> { return x.user.id < 200 }).select(x=>  { return {asdf:x.user.screen_name}}).orderBy(x=> { return x.text })
  // .select(function (x) { return x.user.screen_name + ':' + x.text })
  // .toarray();
  // shortcut! string lambda selector
  // var queryResult2 = jsonArray.where("user.id < 200")
  // .orderBy("$.user.screen_name")
  // .select("$.user.screen_name + ':' + $.text")
  // .toArray();
  LinqFrom(jsonArray).Add({ "user2": { "id": 301, "screen_name": "a_xbox" }, "text": "halo reach" });

  LinqFrom(jsonArray).Remove({ "user2": { "id": 301, "screen_name": "a_xbox" }, "text": "halo reach" });

  var bl = LinqFrom(jsonArray).Any(x => x.text == 'ga');

  var asd = ObjectUtil.isEquals({ a: 2 }, { a: 1 });
  console.log('ObjectUtil.isEquals', asd)
  console.log('queryResultbbb', bl)
  console.log('Contains', LinqFrom(jsonArray).Select(x => x.text))
  var ss = jsonArray.map(x => x.text)

  console.log('map', ss)

}


