import Vue from 'vue'
import Router from '../router/index'
 
class baseVms{

    constructor(){
        this.baseVue=new Vue();
        this.baseRoute=Router;
    }
    baseRoute;
    baseVue;
}

export const baseVm=function(){
    return new baseVms();
}

