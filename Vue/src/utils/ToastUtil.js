import bus from './bus';

const Message = function (msg, type) {
    bus.$message({
        message: msg,
        type: type
      });
}


class Toast {
    /**消息提示 默认警告*/
     Tip(message, typestring) {
        if (!message) {
            return;
        }
        if (typestring == "success") {
            Message(message,'success')
            return;
        }
        if (typestring == "error") {
            Message(message,'error')
            return;
        }
        Message(message,'warning')
    }
    /**成功提示 */
    Success(message){
        this.Tip(message, "success") ;
    }
    /**失败提示 */
    Error(message){
        this.Tip(message, "error") ;
    }


}

export default  new Toast;