import Vue from 'vue';

// 使用 Event Bus
const bus = new Vue();

const temp = {
    val: {},
    consul: []
}

Vue.prototype.setTemp = function (key, val) {
    temp.consul[key] = val;
}

Vue.prototype.getTemp = function (key) {
    return temp.consul[key];
}

Vue.prototype.getTempAll = function () {
    return temp.consul;
}

export default bus;