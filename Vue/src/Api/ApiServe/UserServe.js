import { UserAction} from "../action/ApiAction";
import cache from "../../utils/cacheUtil";
import { BaseServe } from "./BaseServe";

class UserServe {
    constructor() {
        this.BaseServe = new BaseServe(UserAction);
    }

    async LoginUser(data, succesBack, errorBack) {
         let postData={
            Secret:data.Secret,
            Phone:data.Phone,
            Password:data.Password,
            Account:"1",
            Name:"1",
            typeid:'f86cef53-61b9-4f0c-bf4a-f740e93fe288'
         };
        let response = await this.BaseServe.BaseSend.Send(UserAction.Login,postData); 
        response.getValidMessage("登录成功");
        if(response.getIsOk()){
            cache.setAccount(postData.Phone);
            cache.setPasswrod(postData.Password);
            cache.setToken(response.getdata().token);
            cache.setUserInfo(response.getdata().userinfo);
            cache.setSider(response.getdata().resources);
            succesBack(response.getdata());
        }else{
            cache.clear();
            errorBack(response);
        }
    }

    async Register(data, succesBack, errorBack) {
       let response = await this.BaseServe.BaseSend.Send(UserAction.Register,data); 
       response.getValidMessage("注册成功");
       if(response.getIsOk()){
           succesBack(response.getdata());
       }else{
           errorBack(response);
       }
   }

}

/**
 * spend类服务函数
 */
export default new UserServe;