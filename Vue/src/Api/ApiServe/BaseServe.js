import { BaseSend, PageParam } from "../stdlib/ApiBaseSend";
import ObjectUtil from "../../utils/ObjUtil";

/**
 * 封装crud
 */
export class BaseServe {

    constructor(ApiActionObj) {
        this.apiAction = ApiActionObj;
        this.BaseSend=new BaseSend();
    }

    /**获取分页对象 */
    getPageData(index, size, Param) {
        return new PageParam(index, size, Param);
    }

    /**所有 */
    async GetAll(succesBack, errorBack) {
        let response = await this.BaseSend.Send(this.apiAction.GetAll);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }


    /**分页获取 */
    async GetPageList(data, succesBack, errorBack) {
        let response = await this.BaseSend.Send(this.apiAction.GetList, data);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }
    /**添加 */
    async Add(data, succesBack, errorBack) {
        let response = await this.BaseSend.Send(this.apiAction.Add, data);
        response.getValidMessage("保存成功");
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }
    /**删除 */
    async Delete(ids, succesBack) {
        let response = await this.BaseSend.Send(this.apiAction.Delete, ids);
        response.getValidMessage("删除成功");
        response.getIsOk() ? succesBack(response.getdata()) : null;
    }
    /**修改 */
    async Update(data, succesBack, errorBack) {
        let response = await this.BaseSend.Send(this.apiAction.Update, data);
        response.getValidMessage("修改成功");
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }


    /**修改或者添加,按住键判断 */
    async AddOrUpdate(data, succesBack, errorBack) {
        if (!ObjectUtil.IsNullOrWhiteSpace(data.Id)) {
            this.Update(data, succesBack, errorBack);
        } else {
            this.Add(data, succesBack, errorBack);
        }
    }
     /**获取1个 */
     async GetById(id, succesBack, errorBack) {
        let response = await this.BaseSend.Send(this.apiAction.GetById, {id:id},true);
        response.getValidMessage();
        response.getIsOk() ? succesBack(response.getdata()) : errorBack(response);
    }

}

