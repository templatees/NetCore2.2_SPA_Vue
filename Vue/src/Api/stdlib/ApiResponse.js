
 import Toast from '../../utils/ToastUtil';
import ObjectUtil from "../../utils/ObjUtil";
import ApiConfig from "../ApiConfig";
 

/**采用 闭包 封装 私有实例*/
class HttpResponse {
    constructor(data) {
        let __res__ = data.data;
        this.setMsg = (data) => {
            __res__[ApiConfig.ApiConst.MSG] = data;
            return this;
        }
        this.setdata = (data) => {
            __res__[ApiConfig.ApiConst.DATA] = data;
            return this;
        }
        this.setIsOk = (data) => {
            __res__[ApiConfig.ApiConst.ISOK] = data;
            return this;
        }
        this.getCode = () => {
            return __res__[ApiConfig.ApiConst.STATUS];
        }
        this.getMsg = () => {
            return __res__[ApiConfig.ApiConst.MSG];
        }
        this.getdata = () => {
            return __res__[ApiConfig.ApiConst.DATA];
        }
        this.getIsOk = () => {
            return __res__[ApiConfig.ApiConst.ISOK];
        }
        /**如果错误则提示，若果成功 可定义成功提示 */
        this.getValidMessage = (successmsg) => {
            if (!this.getIsOk()) {
                Toast.Error(this.getMsg());
                return this;
            }
            if (successmsg) {
                Toast.Success(successmsg);
                return this;
            }
        }
    }
}


export function createResponse(req) {
    if (!ObjectUtil.isObject(req)) {
        throw " 非OBJ";
       // return
    }
    return new HttpResponse(req);
}
 
 