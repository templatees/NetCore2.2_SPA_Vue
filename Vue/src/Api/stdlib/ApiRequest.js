import requestCommon from "./request";
import ApiConfig from "../ApiConfig";
 

export class ContentType {
    static JSON = 'application/json';
    static FORM = 'application/x-www-form-urlencoded';
    static FLIE = 'multipart/form-data';//文件上载
}

export class HttpMethod {
    static GET = 'GET';
    static POST = 'POST';
}
/**表示服务器响应的数据类型 */
export class ResponseType {
    static JSON = 'json';
    static TEXT = 'text';
    static TEXT = 'stream';
    static TEXT = 'document';
    static TEXT = 'blob';
    static TEXT = 'arraybuffer';
}
 
class HttpRequest {
    requestParams = {
        method: HttpMethod.GET,
        url: '/',
        data: {},
        params: {},
        baseURL: '',
        headers: {
            'Content-Type': ContentType.JSON
        },
        timeout: 200000
    };
    constructor() {
        this.requestParams.baseURL = ApiConfig.baseUrl()
        //   this.requestParams
    }
    /** querystring参数 常用 get方法*/
    setDataParams(value) {
        this.requestParams.params = value;
        return this;
    }
    /** from body 参数 常用 post方法*/
    setDataDatas(value) {
        this.requestParams.data = value;
        return this;
    }

    setContentType(ContentType) {
        this.requestParams.headers["Content-Type"] = ContentType;
        return this;
    }

    /**'Content-Type': 'ContentType.JSON' */
    AddHeaders(obj) {
        this.requestParams.headers = { ...obj, ...this.requestParams.headers };
        return this;
    }


    setUrl(url) {
        this.requestParams.url = url;
        return this;
    }
    setBaseUrl(url) {
        this.requestParams.baseURL = url;
        return this;
    }
    setMethod(method) {
        this.requestParams.method = method;
        return this;
    }
    setTime(method) {
        this.requestParams.timeout = method;
        return this;
    }
    // setHeader(header) {
    //     this.requestParams.header = header;
    //     return this;
    // }
    /**发送请求 */
    Request() {
        //console.log(apiToken.getToken())
        return requestCommon.axios(this.requestParams);
    }
}

// class ResponseProcess {
//     params = {
//         validate: null,//验证数据函数
//         process: null,//处理数据函数,
//         after: null
//     }

//     setValidate(validate) {
//         this.params.validate = validate;
//         return this;
//     }

//     validate(data) {
//         if (typeof this.params.validate == 'function') {
//             return this.params.validate(data);
//         } else {
//             return true;
//         }
//     }

//     setProcess(process) {
//         this.params.process = process;
//         return this;
//     }

//     process(data) {
//         if (typeof this.params.process == 'function') {
//             return this.params.process(data);
//         } else {
//             return data;
//         }
//     }

// }



 



export { HttpRequest };
