const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const domainConfig = require('./ClientApp/domainConfig.js');
var wwwroot= domainConfig.wwwroot; 

module.exports = (env) => {
    const isDevBuild = !(env && env.prod);
    const extractCSS = new ExtractTextPlugin('vendor.css');

    return [{
        stats: { modules: false },
        resolve: { extensions: [ '.js' ] },
        entry: {
            vendor: [
                   "babel-polyfill",
                "event-source-polyfill",
                "vue",
               "vue-property-decorator",
                "vue-router",
                "vue-template-compiler",
                "vuex",
                "bootstrap",
                "element-ui",
                "jquery",
                "nprogress",
                "v-click-outside",
                "vue2-scrollbar",
                'bootstrap/dist/css/bootstrap.css',

"echarts",
"file-saver"
               ],
        },
        module: {
            rules: [
                { test: /\.css(\?|$)/, use: extractCSS.extract({ use: isDevBuild ? 'css-loader' : 'css-loader?minimize' }) },
                { test: /\.(png|woff|woff2|eot|ttf|svg)(\?|$)/, use: 'url-loader?limit=100000' }
            ]
        },
        output: { 
            path: path.join(__dirname, '../WLYD.TMS.OperationSPA/wwwroot', wwwroot),
            publicPath: wwwroot+'/',
            filename: '[name].js',
            library: '[name]_[hash]'
        },
        plugins: [
            extractCSS,
            new webpack.ProvidePlugin({ $: 'jquery', jQuery: 'jquery' }), // Maps these identifiers to the jQuery package (because Bootstrap expects it to be a global variable)
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': isDevBuild ? '"development"' : '"production"'
            }),
            new webpack.DllPlugin({
                path: path.join(__dirname, '../WLYD.TMS.OperationSPA/wwwroot', wwwroot, '[name]-manifest.json'),
                name: '[name]_[hash]'
            })
        ].concat(isDevBuild ? [] : [
            new webpack.optimize.UglifyJsPlugin()
        ])
    }];
};
