// const path = require('path');
// const webpack = require('webpack');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
 
// var wwwroot= 'qqqqq'; 

// const bundleOutputDir = '../WLYD.TMS.OperationSPA/wwwroot/'+wwwroot;

// module.exports = (env) => {
//     const isDevBuild = !(env && env.prod);
//     return [{
//         stats: { modules: false },

//         context: __dirname,
//         resolve: { 
//             extensions: [ '.js', '.ts','.vue','.vue.html', '.css' ],
//             alias: {
               
//             }
//         },
//         entry: { 'main': ["babel-polyfill", './src/main.js'] },
//         externals: { 
//             'AMap': 'AMap'
//         },
//         module: {
//             rules: [
//                 { test: /\.vue|\.vue\.html$/, include: /ClientApp/, loader: 'vue-loader', options: {} },
//                 { test: /\.js$/, include: /ClientApp/, use: 'babel-loader' },
//                 { test: /\.ts$/, include: /ClientApp/, use: 'awesome-typescript-loader?silent=true' },
           
//                 { test: /\.css$/, use: isDevBuild ? ['style-loader', 'css-loader'] : ExtractTextPlugin.extract({ use: 'css-loader?minimize', publicPath:'/'+wwwroot+'/' }) },
//                 { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' },
//                 { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
//                 { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }
//             ]
//         },
      
//         output: {
//             path: path.join(__dirname, bundleOutputDir),
//             filename: '[name].js',
//             publicPath: wwwroot+'/'
//         },
//         plugins: [
//  new webpack.ProvidePlugin({
//             $: "jquery",
//             jQuery: "jquery",
//             jquery: "jquery",
//             "window.jQuery": "jquery"
//         }),
//             new CheckerPlugin(),
//             new webpack.DefinePlugin({
//                 'process.env': {
//                     NODE_ENV: JSON.stringify(isDevBuild ? 'development' : 'production')
//                 }
//             }),
          
//        //     new CopyWebpackPlugin([
//         //        {
//         //           from: './/ClientApp/static',
//          //           to: './'
//         //        },

//         //    ]),
// //, {
//             //        ignore: [],
//             //        copyUnmodified: true,
//             //        debug: "debug"
//             //    })
//         ].concat(isDevBuild ? [
//             // Plugins that apply in development builds only
//             new webpack.SourceMapDevToolPlugin({
//                 filename: '[file].map', // Remove this line if you prefer inline source maps
//                 moduleFilenameTemplate: path.relative(bundleOutputDir, '[resourcePath]') // Point sourcemap entries to the original file locations on disk
//             })
//         ] : [
//             // Plugins that apply in production builds only
//                 new webpack.optimize.UglifyJsPlugin(),
//                 new ExtractTextPlugin('site.css')
//         ])
//     }];
// };


var path = require('path')
var webpack = require('webpack')

var VueLoaderPlugin = require('vue-loader/lib/plugin')

const WebPackConfig = {
    path:'../wwwroot',
    name:'dist',
    url:'../wwwroot/dist',
    filename:'[name].js',
 };

 

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, WebPackConfig.url),
    publicPath: '/'+WebPackConfig.name+'/',
    filename: WebPackConfig.filename
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      }, {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options:{
          plugins:['syntax-dynamic-import']
          },
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
        loader: 'file-loader'
      }//elemenui要加
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
}

module.exports.plugins=[
  new VueLoaderPlugin()
]

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
      
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}